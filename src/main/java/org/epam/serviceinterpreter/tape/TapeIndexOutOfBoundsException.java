package org.epam.serviceinterpreter.tape;

public class TapeIndexOutOfBoundsException extends RuntimeException {

    TapeIndexOutOfBoundsException(String message) {
        super(message);
    }
}