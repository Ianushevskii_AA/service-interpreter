package org.epam.serviceinterpreter.tape;

import java.util.ArrayList;
import java.util.List;

public class TuringTapeImpl implements TuringTape {

    private static final char DEFAULT_CHAR = '\0';
    private static final int START_SIZE = 100;
    private static final int MAX_SIZE = 30_000;

    private final List<Character> leftTape = new ArrayList<>(START_SIZE);
    private final List<Character> rightTape = new ArrayList<>(START_SIZE);

    private int position = 0;

    public TuringTapeImpl() {
        rightTape.add(DEFAULT_CHAR);
    }

    private int leftTapePosition() {
        return Math.abs(position) - 1;
    }

    private void ensureValueSet() {
        if (position < 0 && leftTapePosition() == leftTape.size()) {
            leftTape.add(DEFAULT_CHAR);
        } else if (position == rightTape.size()) {
            rightTape.add(DEFAULT_CHAR);
        }
    }

    @Override
    public void writeRowValue(char value) {
        ensureValueSet();
        if (position < 0) {
            leftTape.set(leftTapePosition(), value);
        } else {
            rightTape.set(position, value);
        }
    }

    @Override
    public char getRowValue() {
        ensureValueSet();
        if (position < 0) {
            return leftTape.get(leftTapePosition());
        } else {
            return rightTape.get(position);
        }
    }

    private void tapeSizeExceeded() {
        throw new TapeIndexOutOfBoundsException(position + " tape head position is out of bounds");
    }

    @Override
    public void gotoNextRow() {
        ++position;
        if (position == MAX_SIZE) tapeSizeExceeded();
    }

    @Override
    public void gotoPreviousRow() {
        --position;
        if (position < 0 && leftTapePosition() == MAX_SIZE) tapeSizeExceeded();
    }

    @Override
    public void increaseRowValueByOne() {
        char value = getRowValue();
        writeRowValue(++value);
    }

    @Override
    public void reduceRowValueByOne() {
        char value = getRowValue();
        writeRowValue(--value);
    }
}