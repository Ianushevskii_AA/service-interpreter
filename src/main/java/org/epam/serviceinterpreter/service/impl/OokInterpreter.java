package org.epam.serviceinterpreter.service.impl;

import java.util.ArrayList;
import java.util.List;
import org.epam.serviceinterpreter.service.Interpreter;
import org.epam.serviceinterpreter.tape.TuringTape;

public class OokInterpreter implements Interpreter {

    List<OokCommand> commandList;
    TuringTape turingTape;
    StringBuilder input;
    String out;
    int commandPointer;

    public OokInterpreter(TuringTape turingTape){
        this.commandList = new ArrayList<>();
        this.turingTape = turingTape;
        this.out = "";
        this.commandPointer = 0;
    }

    @Override
    public void loadProgram(String programText) {
        int pointer = 0;


        while(pointer < programText.length()){
            String command;
            boolean validCommand = false;

            if (pointer + 9 <= programText.length()){
                command = programText.substring(pointer, pointer+9);
            } else {
                command = programText.substring(pointer, pointer + (programText.length() - pointer));
            }

            for (OokCommand observingCommand: OokCommand.values()){
                if (observingCommand.getCommand().equals(command)){
                    commandList.add(observingCommand);
                    pointer+=10;
                    validCommand = true;
                    break;
                }
            }

            if (!validCommand){
                pointer += (pointer+ 10 > programText.length())? (programText.length() - pointer) : 1;
            }

        }


    }

    @Override
    public void loadKeyboardInput(String pressedKeys) {
        this.input = new StringBuilder(pressedKeys);
    }

    @Override
    public boolean hasNextCommand() {
        return commandPointer < commandList.size();
    }

    @Override
    public void executeNextCommand() {
        OokCommand command = commandList.get(commandPointer);
        switch(command){
            case NEXT:
                turingTape.gotoNextRow();
                break;
            case PREV:
                turingTape.gotoPreviousRow();
                break;
            case IN:
                if (input.length() > 0) {
                    turingTape.writeRowValue(input.charAt(0));
                    input.deleteCharAt(0);
                }
                break;
            case OUT:
                this.out += (char)turingTape.getRowValue();
                break;
            case INCR:
                turingTape.increaseRowValueByOne();
                break;
            case DECR:
                turingTape.reduceRowValueByOne();
                break;
            case START:
                if (turingTape.getRowValue()==0){
                    int position = 1;
                    while(position > 0){
                        commandPointer++;

                        if (commandList.get(commandPointer).equals(OokCommand.START))
                            position++;
                        else if (commandList.get(commandPointer).equals(OokCommand.END))
                            position--;
                    }
                }
                break;
            case END:
                if (turingTape.getRowValue() != 0){
                    int position = 1;
                    while(position > 0){
                        commandPointer--;

                        if (commandList.get(commandPointer).equals(OokCommand.START))
                            position--;
                        else if (commandList.get(commandPointer).equals(OokCommand.END))
                            position++;
                    }
                }
                break;
        }
        commandPointer++;
    }

    @Override
    public String getResult() {
        return this.out;
    }
}
