package org.epam.serviceinterpreter.service.impl;

import org.epam.serviceinterpreter.service.Interpreter;
import org.epam.serviceinterpreter.tape.TuringTape;

import java.util.Arrays;
import java.util.LinkedList;

public class InterpreterBF implements Interpreter {

    private String programText, pressedKeys;
    private int dataPointer, loopCounter ;
    private TuringTape turingTape;
    private StringBuilder result;
    private LinkedList<Character> commands = new LinkedList<>(Arrays.asList('+','-','>','<','[',']','.',','));

    public InterpreterBF(TuringTape turingTape){
        this.turingTape = turingTape;
        result = new StringBuilder();
        loopCounter = 0;
    }

    @Override
    public void loadProgram(String programText){
        this.programText = programText;
        dataPointer = 0;
    }

    @Override
    public void loadKeyboardInput(String pressedKeys){
        if (pressedKeys!= null){
            this.pressedKeys = pressedKeys;
        }
        else {
            throw new NullPointerException("There is no pressed keys");
        }
    }

    @Override
    public boolean hasNextCommand(){
        while (programText.length() > dataPointer) {
            if (commands.contains(programText.charAt(dataPointer))){
                return true;
            }
            else{
                dataPointer ++;
            }
        }
        return false;
    }


    @Override
    public void executeNextCommand(){
        if (this.hasNextCommand()){
            switch (programText.charAt(dataPointer)) {
                case '+':
                    turingTape.increaseRowValueByOne();
                    break;
                case '-':
                    turingTape.reduceRowValueByOne();
                    break;
                case '>':
                    turingTape.gotoNextRow();
                    break;
                case '<':
                    turingTape.gotoPreviousRow();
                    break;
                case '.':
                    result.append(turingTape.getRowValue());
                    break;
                case ',':
                    if (pressedKeys.length()!=0) {
                        turingTape.writeRowValue(pressedKeys.charAt(0));
                        pressedKeys = pressedKeys.substring(1);
                    }
                    else {
                        throw new RuntimeException("Number of keys entered is less than required");
                    }
                    break;
                case '[':
                    if (turingTape.getRowValue()==0){
                        loopCounter = 1;
                        while(loopCounter>0){
                            dataPointer++;
                            if (dataPointer==programText.length()) {
                                throw new RuntimeException("Unclosed bracket");
                            }
                            if (programText.charAt(dataPointer)=='[')
                                loopCounter++;
                            else if (programText.charAt(dataPointer)==']')
                                loopCounter--;
                        }
                    }
                    break;
                case ']':
                    if (turingTape.getRowValue()!=0) {
                        loopCounter = 1;
                        while (loopCounter > 0) {
                            dataPointer--;
                            if(dataPointer == -1){
                                throw new RuntimeException("Unexpected closing bracket");
                            }
                            if (programText.charAt(dataPointer) == '[')
                                loopCounter--;
                            else if (programText.charAt(dataPointer) == ']')
                                loopCounter++;
                        }
                    }
                    break;
            }
            dataPointer++;
        }
    }

    @Override
    public String getResult(){
        return result.toString();
    }
}
