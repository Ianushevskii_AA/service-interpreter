/**
 * Created by alevtina on 15/02/2019.
 */
package org.epam.serviceinterpreter.service.impl;

import java.util.ArrayList;
import org.epam.serviceinterpreter.service.Interpreter;
import org.epam.serviceinterpreter.tape.TuringTape;

public class CowInterpreter implements Interpreter {

    TuringTape tape;

    private String keyboardInput;
    private int commandPositionCounter;

    private StringBuilder output;
    private ArrayList<Integer> program;

    private Character register;

    public CowInterpreter(TuringTape turingTape) {
        this.tape = turingTape;
        this.commandPositionCounter = 0;
        this.output = new StringBuilder();
        this.program = new ArrayList<>();
    }

    @Override
    public void loadProgram(String programText) {
        String remainingProgramText = programText;

        while (remainingProgramText.length() >= 3) {
            String command = remainingProgramText.substring(0, 3);
            switch (command) {
                case "moo":
                    program.add(0);
                    remainingProgramText = remainingProgramText.substring(3);
                    break;
                case "mOo":
                    program.add(1);
                    remainingProgramText = remainingProgramText.substring(3);
                    break;
                case "moO":
                    program.add(2);
                    remainingProgramText = remainingProgramText.substring(3);
                    break;
                case "mOO":
                    program.add(3);
                    remainingProgramText = remainingProgramText.substring(3);
                    // FIXME: third command does not exist yet
                    break;
                case "Moo":
                    program.add(4);
                    remainingProgramText = remainingProgramText.substring(3);
                    break;
                case "MOo":
                    program.add(5);
                    remainingProgramText = remainingProgramText.substring(3);
                    break;
                case "MoO":
                    program.add(6);
                    remainingProgramText = remainingProgramText.substring(3);
                    break;
                case "MOO":
                    program.add(7);
                    remainingProgramText = remainingProgramText.substring(3);
                    break;
                case "OOO":
                    program.add(8);
                    remainingProgramText = remainingProgramText.substring(3);
                    break;
                case "MMM":
                    program.add(9);
                    remainingProgramText = remainingProgramText.substring(3);
                    break;
                case "OOM":
                    program.add(10);
                    remainingProgramText = remainingProgramText.substring(3);
                    break;
                case "oom":
                    program.add(11);
                    remainingProgramText = remainingProgramText.substring(3);
                    break;
                default:
                    remainingProgramText = remainingProgramText.substring(1);
            }
        }
    }

    @Override
    public void loadKeyboardInput(String pressedKeys) {
        this.keyboardInput = pressedKeys;
    }

    @Override
    public boolean hasNextCommand() {
        return commandPositionCounter < program.size();
    }

    @Override
    public void executeNextCommand() {
        switch (program.get(commandPositionCounter)) {
            case 0:
                processEndOfLoop();
                break;
            case 1:
                goToPreviousRow();
                break;
            case 2:
                goToNextRow();
                break;
            case 3:
                break;
            case 4:
                writeTapeValueIfZeroOrReadValue();
                break;
            case 5:
                decrementValueByOne();
                break;
            case 6:
                incrementValueByOne();
                break;
            case 7:
                processBeginningOfLoop();
                break;
            case 8:
                setValueToZero();
                break;
            case 9:
                copyValueIfRegisterIsEmptyOrPasteValueOfRegister();
                break;
            case 10:
                readTapeRowValue();
                break;
            case 11:
                writeTapeRowValue();
                break;
        }
        commandPositionCounter++;
    }

    @Override
    public String getResult() {
        return String.valueOf(output);
    }

    private void processEndOfLoop() {
        int loopLevel = 1;
        while (loopLevel > 0) {
            commandPositionCounter--;
            if (program.get(commandPositionCounter) == 7) {
                loopLevel --;
            } else if (program.get(commandPositionCounter) == 0){
                loopLevel++;
            }
        }
        commandPositionCounter--;
    }

    private void goToPreviousRow() {
        tape.gotoPreviousRow();
    }

    private void goToNextRow() {
        tape.gotoNextRow();
    }

    private void writeTapeValueIfZeroOrReadValue() {
        if (tape.getRowValue() == 0) {
            writeTapeRowValue();
        } else {
            readTapeRowValue();
        }
    }

    private void decrementValueByOne() {
        tape.reduceRowValueByOne();
    }

    private void incrementValueByOne() {
        tape.increaseRowValueByOne();
    }

    private void processBeginningOfLoop() {
        if (tape.getRowValue() == 0) {
            int loopLevel = 1;
            while (loopLevel > 0) {
                commandPositionCounter ++;
                if (program.get(commandPositionCounter) == 7) {
                    loopLevel++;
                } else if (program.get(commandPositionCounter) == 0) {
                    loopLevel--;
                }
            }
        }
    }

    private void setValueToZero() {
        char zero = 0;
        tape.writeRowValue(zero);
    }

    private void copyValueIfRegisterIsEmptyOrPasteValueOfRegister() {
        if (register == null) {
            register = tape.getRowValue();
        } else {
            tape.writeRowValue(register);
            register = null;
        }
    }

    private void readTapeRowValue() {
        output.append(tape.getRowValue());
    }

    private void writeTapeRowValue() {
        char currentInput = 0;
        try {
            currentInput = readCharacterFromInput();
        } catch (Exception e) {
            e.printStackTrace();
        }
        tape.writeRowValue(currentInput);
    }

    private char readCharacterFromInput() throws RuntimeException {
        if (keyboardInput.length() > 0) {
            char currentInput = keyboardInput.charAt(0);
            keyboardInput = keyboardInput.substring(1);
            return currentInput;
        } else {
            throw new RuntimeException("Cannot load char from input.");
        }
    }
}
