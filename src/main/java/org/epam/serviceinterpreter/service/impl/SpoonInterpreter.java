package org.epam.serviceinterpreter.service.impl;

import org.epam.serviceinterpreter.service.Interpreter;
import org.epam.serviceinterpreter.tape.TuringTape;

import java.util.HashMap;

/**
 * Implements the Spoon interpreter
 */
public class SpoonInterpreter implements Interpreter {

    /**
     * Interface for commands
     */
    private interface Instruction {
        void execute(TuringTape turingTape);
    }

    private HashMap<String, Instruction> commands = new HashMap<>();
    private TuringTape turingTape;
    private StringBuilder result;
    private String pressedKeys, programText, currentCommand;
    private HashMap<Integer, Integer> commandLocation;
    private int commandPtr = 0, textPtr = 0, currentPressedKey = 0;

    public SpoonInterpreter(TuringTape turingTape) {
        this.turingTape = turingTape;
        commandsInit();
        result = new StringBuilder();
        commandLocation = new HashMap<>();
    }

    /**
     * Matches sequences of characters and their corresponding commands
     */
    private void commandsInit() {
        commands.put("1", TuringTape::increaseRowValueByOne);
        commands.put("000", TuringTape::reduceRowValueByOne);
        commands.put("010", TuringTape::gotoNextRow);
        commands.put("011", TuringTape::gotoPreviousRow);
        commands.put("0011", turingTape -> endOfLoop());
        commands.put("00100", turingTape -> startOfLoop());
        commands.put("001010", turingTape -> result.append(turingTape.getRowValue()));
        commands.put("0010110", turingTape -> inputCharacter());
    }

    private void startOfLoop() {
        if ((int) turingTape.getRowValue() == 0) {
            int loopCounter = 1;
            while (loopCounter > 0 && hasNextCommand()) {
                if (!commandLocation.containsKey(commandPtr + 1)) {
                    throw new RuntimeException("The loop will not be executed");
                }
                textPtr = commandLocation.get(++commandPtr);
                currentCommand = buildCommand();
                if (currentCommand.equals("00100")) {
                    loopCounter++;
                } else if (currentCommand.equals("0011")) {
                    loopCounter--;
                }
            }
            if (commandLocation.containsKey(commandPtr)) {
                textPtr = commandLocation.get(commandPtr);
                textPtr--;
            }
            commandPtr--;
        }
    }

    private void endOfLoop() {
        if ((int) turingTape.getRowValue() > 0) {
            int loopCounter = 1;
            while (loopCounter > 0 && hasNextCommand()) {
                if (!commandLocation.containsKey(commandPtr - 1)) {
                    throw new RuntimeException("The loop will not be executed");
                }
                textPtr = commandLocation.get(--commandPtr);
                currentCommand = buildCommand();
                if (currentCommand.equals("00100")) {
                    loopCounter--;
                } else if (currentCommand.equals("0011")) {
                    loopCounter++;
                }
            }
            textPtr = commandLocation.get(commandPtr);
            textPtr--;
            commandPtr--;
        }
    }

    private void inputCharacter() {
        if (currentPressedKey < pressedKeys.length()) {
            turingTape.writeRowValue(pressedKeys.charAt(currentPressedKey));
            currentPressedKey++;
        } else {
            throw new RuntimeException("The character is not entered");
        }
    }

    /**
     * @return the new command
     */
    private String buildCommand() {
        int maxCommandLength = 7;
        StringBuilder newCommand = new StringBuilder();
        for (int i = 1; i <= maxCommandLength; i++) {
            if (programText.charAt(textPtr) == '0' || programText.charAt(textPtr) == '1') {
                newCommand.append(programText.charAt(textPtr));
            }
            if (commands.containsKey(newCommand.toString())) {
                break;
            } else {
                textPtr++;
                if (i == maxCommandLength || textPtr == programText.length()) {
                    throw new RuntimeException("Unknown instruction");
                }
            }
        }
        return newCommand.toString();
    }

    @Override
    public void loadProgram(String programText) {
        this.programText = programText.replaceAll("[^01]", "");
    }

    @Override
    public void loadKeyboardInput(String pressedKeys) {
        this.pressedKeys = pressedKeys;
    }

    @Override
    public boolean hasNextCommand() {
        return textPtr < programText.length();
    }

    @Override
    public void executeNextCommand() {
        currentCommand = buildCommand();
        commandPtr++;
        commands.get(currentCommand).execute(turingTape);
        textPtr++;
        if (!commandLocation.containsKey(commandPtr)) {
            commandLocation.put(commandPtr, textPtr - currentCommand.length());
        }
    }

    @Override
    public String getResult() {
        return result.toString();
    }

}
