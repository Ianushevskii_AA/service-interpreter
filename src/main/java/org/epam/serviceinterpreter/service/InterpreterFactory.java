package org.epam.serviceinterpreter.service;

import org.epam.serviceinterpreter.exception.InterpreterLangUnknownException;
import org.epam.serviceinterpreter.service.impl.CowInterpreter;
import org.epam.serviceinterpreter.service.impl.InterpreterBF;
import org.epam.serviceinterpreter.service.impl.OokInterpreter;
import org.epam.serviceinterpreter.service.impl.SpoonInterpreter;
import org.epam.serviceinterpreter.tape.TuringTapeImpl;
import org.springframework.stereotype.Component;

@Component
public class InterpreterFactory {
    
    public Interpreter createInterpreter(String name) {
        String nameLowerCase = name.toLowerCase();

        switch (nameLowerCase){
            case "brainfuck":
                return new InterpreterBF(new TuringTapeImpl());
            case "cow":
                return new CowInterpreter(new TuringTapeImpl());
            case "ook":
                return new OokInterpreter(new TuringTapeImpl());
            case "spoon":
                return new SpoonInterpreter(new TuringTapeImpl());
            default:
                throw new InterpreterLangUnknownException(name);
        }
    }
}
