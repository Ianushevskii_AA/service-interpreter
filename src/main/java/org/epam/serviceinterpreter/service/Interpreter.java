package org.epam.serviceinterpreter.service;

public interface Interpreter {

    void loadProgram(String programText);

    void loadKeyboardInput(String pressedKeys);

    boolean hasNextCommand();

    void executeNextCommand();

    String getResult();
}
