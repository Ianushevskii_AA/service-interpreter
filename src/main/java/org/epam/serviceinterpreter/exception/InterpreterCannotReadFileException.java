package org.epam.serviceinterpreter.exception;

public class InterpreterCannotReadFileException extends RuntimeException {
    public InterpreterCannotReadFileException(String msg){
        super(msg);
    }
}
