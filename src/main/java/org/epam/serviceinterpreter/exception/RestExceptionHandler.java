package org.epam.serviceinterpreter.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.io.IOException;
import java.util.Date;

@ControllerAdvice
public class RestExceptionHandler extends ResponseEntityExceptionHandler{

    @ExceptionHandler(InterpreterLangUnknownException.class)
    public ResponseEntity<ErrorDetails> handleLangUnknownException(InterpreterLangUnknownException e){
        ErrorDetails errorDetails = new ErrorDetails(new Date(),"Language not found",e.getMessage());
        return new ResponseEntity<>(errorDetails, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(InterpreterLoadProgramException.class)
    public ResponseEntity<ErrorDetails> handleLoadProgramException(InterpreterLoadProgramException e){
        ErrorDetails errorDetails = new ErrorDetails(new Date(),"Program file cannot be loaded",e.getMessage());
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InterpreterLoadKeyboardInputException.class)
    public ResponseEntity<ErrorDetails> handleLoadKeyboardInputException(InterpreterLoadKeyboardInputException e){
        ErrorDetails errorDetails = new ErrorDetails(new Date(),"Keyboard file cannot be loaded",e.getMessage());
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(InterpreterProgramExecutionException.class)
    public ResponseEntity<ErrorDetails> handleProgramExecutionException(InterpreterProgramExecutionException e){
        ErrorDetails errorDetails = new ErrorDetails(new Date(),"Program failed during the execution",e.getMessage());
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler (InterpreterCannotReadFileException.class)
    public ResponseEntity<ErrorDetails> handleIOException(InterpreterCannotReadFileException e){
        ErrorDetails errorDetails = new ErrorDetails(new Date(),"Error: Input file(-s) cannot be read", e.getMessage());
        return new ResponseEntity<>(errorDetails, HttpStatus.BAD_REQUEST);
    }
}
