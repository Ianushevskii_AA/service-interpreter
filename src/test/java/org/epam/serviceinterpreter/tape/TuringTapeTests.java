package org.epam.serviceinterpreter.tape;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

class TuringTapeTests {

    private static final char DEFAULT_CHAR = '\0';
    private static final char VALUE = 'a';

    private final TuringTape turingTape = new TuringTapeImpl();

    @Test
    void getRowValue_DefaultValue() {
        assertEquals(DEFAULT_CHAR, turingTape.getRowValue());
    }

    @Test
    void getRawValue_SpecificValue() {
        turingTape.writeRowValue(VALUE);
        assertEquals(VALUE, turingTape.getRowValue());
    }

    @Test
    void reduceRowValue_NonZeroValue() {
        turingTape.writeRowValue(VALUE);
        turingTape.reduceRowValueByOne();
        assertEquals((char) (VALUE - 1), turingTape.getRowValue());
    }

    @Test
    void reduceRowValue_ZeroValue() {
        turingTape.writeRowValue(Character.MIN_VALUE);
        turingTape.reduceRowValueByOne();
        assertEquals(Character.MAX_VALUE, turingTape.getRowValue());
    }

    @Test
    void increaseRowValue_ZeroValue() {
        turingTape.writeRowValue(Character.MIN_VALUE);
        turingTape.increaseRowValueByOne();
        assertEquals((char) (Character.MIN_VALUE + 1), turingTape.getRowValue());
    }

    @Test
    void increaseRowValue_MaxValue() {
        turingTape.writeRowValue(Character.MAX_VALUE);
        turingTape.increaseRowValueByOne();
        assertEquals(Character.MIN_VALUE, turingTape.getRowValue());
    }

    @Test
    void gotoNextRow_30_000Times() {
        for (int i = 0; i < 30_000 - 1; i++) {
            turingTape.gotoNextRow();
        }
        assertThrows(TapeIndexOutOfBoundsException.class, turingTape::gotoNextRow);
    }

    @Test
    void gotoPreviousRow_30_001Times() {
        for (int i = 0; i < 30_000; i++) {
            turingTape.gotoPreviousRow();
        }
        assertThrows(TapeIndexOutOfBoundsException.class, turingTape::gotoPreviousRow);
    }
}
