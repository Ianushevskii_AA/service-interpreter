package org.epam.serviceinterpreter.service.impl;

import org.epam.serviceinterpreter.tape.TuringTape;
import org.epam.serviceinterpreter.tape.TuringTapeImpl;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class SpoonInterpreterTests {
    private TuringTape turingTape = new TuringTapeImpl();
    private SpoonInterpreter spoonInterpreter = new SpoonInterpreter(turingTape);

    @Test
    void outputSuccess() {
        spoonInterpreter.loadProgram("001010 001010");
        while (spoonInterpreter.hasNextCommand()) {
            spoonInterpreter.executeNextCommand();
        }
        assertEquals("\0\0", spoonInterpreter.getResult());
    }

    @Test
    void inputSuccess() {
        spoonInterpreter.loadProgram("0010110");
        spoonInterpreter.loadKeyboardInput("a");
        spoonInterpreter.executeNextCommand();
    }

    @Test
    void inputError() {
        spoonInterpreter.loadProgram("0010110");
        spoonInterpreter.loadKeyboardInput("");
        spoonInterpreter.executeNextCommand();
    }

    @Test
    void executeNextCommandError() {
        spoonInterpreter.loadProgram("00");
        while (spoonInterpreter.hasNextCommand()) {
            spoonInterpreter.executeNextCommand();
        }
    }

    @Test
    void loopsTest() {
        spoonInterpreter.loadProgram("1 1 00100 000 0011");
        while (spoonInterpreter.hasNextCommand()) {
            spoonInterpreter.executeNextCommand();
        }
    }

    @Test
    void loopNotIncludedError() {
        spoonInterpreter.loadProgram("00100 0011");
        while (spoonInterpreter.hasNextCommand()) {
            spoonInterpreter.executeNextCommand();
        }
    }

    @Test
    void nestedLoops() {
        spoonInterpreter.loadProgram("1 1 010 1 1 011 00100 000 010 00100 000 0011 011 0011");
        while (spoonInterpreter.hasNextCommand()) {
            spoonInterpreter.executeNextCommand();
        }
    }

    @Test
    void nestedLoopsError() {
        spoonInterpreter.loadProgram("00100 00100 0011 0011");
        while (spoonInterpreter.hasNextCommand()) {
            spoonInterpreter.executeNextCommand();
        }
    }

    @Test
    void startLoopError() {
        spoonInterpreter.loadProgram("00100 1 1");
        while (spoonInterpreter.hasNextCommand()) {
            spoonInterpreter.executeNextCommand();
        }
    }

    @Test
    void endLoopError() {
        spoonInterpreter.loadProgram("1 0011 1 1");
        while (spoonInterpreter.hasNextCommand()) {
            spoonInterpreter.executeNextCommand();
        }
    }

    @Test
    void printA() {
        spoonInterpreter.loadProgram("1111111111111111" +
                "11111111111111111111111111111" +
                "11111111111111111111111111111" +
                "11111111111111111111111001010");
        while (spoonInterpreter.hasNextCommand()) {
            spoonInterpreter.executeNextCommand();
        }
        assertEquals("a", spoonInterpreter.getResult());
    }

    @Test
    void cyclePrintA() {
        spoonInterpreter.loadProgram("1111111111100100010" +
                "11111111101100000" +
                "11010000000001010");
        while (spoonInterpreter.hasNextCommand()) {
            spoonInterpreter.executeNextCommand();
        }
        assertEquals("a", spoonInterpreter.getResult());
    }

    @Test
    void inAndOutFiveCharacters() {
        spoonInterpreter.loadProgram("0010110010001011001000101100" +
                "10001011001000101100100110110110110110010" +
                "10010001010010001010010001010010001010010");
        spoonInterpreter.loadKeyboardInput("Magic");
        while (spoonInterpreter.hasNextCommand()) {
            spoonInterpreter.executeNextCommand();
        }
        assertEquals("Magic", spoonInterpreter.getResult());
    }

    @Test
    void helloWorld() {
        spoonInterpreter.loadProgram("11111111110010001011111110" +
                "101111111111010111010101101101101100000" +
                "110101100101001010010101111111001010001" +
                "010111001010010110010100110111111111111" +
                "111110010100100010101110010100000000000" +
                "000000000010100000000000000000000000000" +
                "010100101001010010001010");
        while (spoonInterpreter.hasNextCommand()) {
            spoonInterpreter.executeNextCommand();
        }
        assertEquals("Hello World!\n", spoonInterpreter.getResult());
    }
}
