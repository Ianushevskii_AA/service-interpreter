package org.epam.serviceinterpreter.service;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyNoMoreInteractions;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class InterpreterAppServiceTest {

  private static final String LANGUAGE = "lang";
  private static final String PROGRAM_TEXT = "program";
  private static final String PASSED_KEYS = "keys";

  private InterpreterFactory interpreterFactory = mock(InterpreterFactory.class);
  private Interpreter interpreter = mock(Interpreter.class);
  private InterpreterAppService service;

  @BeforeEach
  void setUp() {
    service = new InterpreterAppService(interpreterFactory);
    given(interpreterFactory.createInterpreter(anyString())).willReturn(interpreter);
  }

  @AfterEach
  void checkNoMoreInteraction() {
    verify(interpreterFactory).createInterpreter(LANGUAGE);
    verifyNoMoreInteractions(interpreterFactory, interpreter);
  }

  @Test
  void executeWhenLoadProgramException() {
    doThrow(RuntimeException.class).when(interpreter).loadProgram(anyString());
    
    assertThrows(RuntimeException.class, () -> service.execute(LANGUAGE, PROGRAM_TEXT, PASSED_KEYS));
    
    verify(interpreter).loadProgram(PROGRAM_TEXT);
  }
}